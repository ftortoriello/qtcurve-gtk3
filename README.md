# QtCurve-GTK3

Theme for GTK 3 based on Adwaita 3.24.28 which tries to match my custom QtCurve
theme.
Also contains the QtCurve scheme files.

## Installing and configuring the theme

### GTK 3 theme
* Copy the assets directory and the .css files to /usr/share/themes/QtCurve or
  $HOME/.themes/QtCurve
* Copy cfg/gtk-3.0/settings.ini to /etc/gtk-3.0/ or $HOME/.config/gtk-3.0/

### QtCurve style
* Copy cfg/qtcurve/stylerc to $HOME/.config/qtcurve/

### KDE color scheme
* Copy kde/*.colors to $HOME/.local/share/color-schemes/
* Select the color scheme with "kcmshell5 kcm_colors"

### qt5ct color scheme
* Copy qt5ct/*.conf to $HOME/.config/qt5ct/colors/
* Select the custom color scheme with "qt5ct"
* Set the font to "Liberation Sans 11"
* Set "Dialog buttons layout" to "GNOME"
